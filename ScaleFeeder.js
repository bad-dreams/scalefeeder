const SerialPort = require('serialport');
const http = require('http');
const https = require('https');
const path = require('path');
const fs  = require('fs');
const process = require('process');
const Parsers = require('./Parsers.js');

const InputFormat = {
  dummy: -1,
  riceLake: 0,
  avery: 1,
};

// Configuration
let configPath = path.resolve(__dirname, 'config.json');
let configuration = JSON.parse(fs.readFileSync(configPath).toString());
console.log('\n', configuration);

// Globals for keeping track of network information.
// let domain = 'beta.fractrack.it';
let domain = 'dj-portal.fractrack.it';
// let domain = 'epsilon.fractrack.it';
let sessionCookies = '';

console.log(`Domain: ${domain}`);

// Global for keeping track of the currently running serial port instance.
let comHandler;

start(configuration);

async function start(config) {
  // When the serial port loses connection the node concurrency list is empty so
  // node stops completely thinking there is no work left to do. So here we
  // create a new serial port listener before node has a chance to die.
  process.on('beforeExit', () => {
    console.log('ERROR: COM has likely been disconnected, waiting for connection.');
    comHandler = makeComHandler(config.parserFormat, config);

    setTimeout(() => {
      console.log(`Waiting for ${config.comPort}, has it been disconnected?`);
    }, 5000);
  });

  await login(configuration.username, configuration.password);
  comHandler = makeComHandler(config.parserFormat, config);
}

// Construct a new SerialPort object with the appropriate listeners then return
// it.
function makeComHandler(format, config) {
  const handler = {
    busy: false,
    previousWeight: -1,
    accumulator: '',
    serialPort: new SerialPort(config.comPort, { baudRate: 9600 }),
    read: () => {
      const data = comHandler.serialPort.read().toString();
      handler.accumulator += data;

      if (handler.accumulator.indexOf('\n') === -1) {
        // If we don't have a complete line to read yet, just keep
        // writing to accumulator.
        return null;
      }

      const output = handler.accumulator;
      handler.accumulator = '';
      return output;
    },
  };

  // Open errors will be emitted as an error event
  handler.serialPort.on('error', (err) => {
    console.log('Error: ', err.message);
  });

  handler.serialPort.on('readable', async () => {
    let currentWeight = null;

    switch (format) {
      case InputFormat.dummy:
        currentWeight = Parsers.dummy(comHandler);
        break;
      case InputFormat.riceLake:
        currentWeight = Parsers.riceLake(comHandler);
        break;
      case InputFormat.avery:
        currentWeight = Parsers.avery(comHandler);
        break;
      default: break;
    }

    if (handler.busy) {
      console.log('.');
      return;
    }

    // If our value from the parsers isn't valid or is stale, do nothing.
    if (currentWeight === null || handler.previousWeight === currentWeight) {
      return;
    }

    handler.busy = true;
    await pushScaleWeight(currentWeight, config);
    handler.previousWeight = currentWeight;
    handler.busy = false;
  })

  return handler;
}

// Login and return a valid cookie string.
async function login(username, password) {
  return new Promise((resolve, reject) => {
    const postData = JSON.stringify({ username, password });
    const options = {
      hostname: domain,
      // port: 8000,
      path: '/api/epsilon/login',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postData),
      },
    };

    const request = https.request(options, (response) => {
      // Cookies come in as an array so we'll need to convert them to a
      // semi-colon delimited cookie string.
      let cookiesList = response.headers['set-cookie'];
      let cookie = '';

      // Read chunks of data into this variable, then once the response is done
      // set the global sessionCookies and return it.
      let data = '';

      // Build a cookie string from each cookie in the response headers.
      cookiesList.forEach((element) => {
        cookie = cookie + element.substring(0, element.indexOf(';') + 1);
      });

      response.on('data', (chunk) => {
        data += chunk;
      });

      response.on('end', () => {
        data = JSON.parse(data);
        if (data.status === 'ok') {
          console.log(data.message);
          sessionCookies = cookie;
          resolve(cookie);
        }
        else {
          console.log(data.message);
          reject();
        }
      })
    });

    request.on('error', (e) => {
      console.error(e);
      comHandler.busy = false;
      reject();
    });

    request.write(postData);
    request.end();
  });
}

// Push Weight scale to database
async function pushScaleWeight(currentWeight, config) {
  return new Promise((resolve, reject) => {
    // Setup request data.
    let postData = JSON.stringify({ scaleId: config.scaleId, weight: currentWeight });
    let options = {
      hostname: domain,
      // port: 8000,
      path: '/api/epsilon/set_scale_weight',
      method: 'POST',
      headers: {
        'Cookie': sessionCookies,
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postData),
      },
      credentials: 'include',
    };

    // Make the request, read back whether it was a success or not and login
    // if we have to.
    const request = https.request(options, (response) => {
      console.log(`Pushing weight: ${currentWeight}`);
      response.on('data', async (responseData) => {
        console.log('\n' + responseData)

        try {
          const data = JSON.parse(responseData).data;

          // We aren't logged in so let's attempt to login now.
          if (data.status !== 'ok') {
            console.log(data.message);
            console.log('Attempting to login...');

            try {
              await login(config.username, config.password);
            } catch(err) {
              console.error(err);
              comHandler.busy = false;
            }
          }

          // We've logged in okay, so let's try to
          if (data.status === 'ok') {
            console.log(`${data.message} to ${currentWeight} for scaleId: ${config.scaleId}.`);
          }

          comHandler.busy = false;
        } catch(e) {
          console.log(e);
          comHandler.busy = false;
        }

        resolve();
      });
    });

    request.on('error', (err) => {
      console.error('HTTPS request error:');
      console.error(err);
      comHandler.busy = false;
      reject();
    });

    request.write(postData);
    request.end();
  });
}
