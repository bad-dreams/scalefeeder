function dummy(comHandler) {
  const data = comHandler.read();
  if (data === null) {
    return null;
  }

  console.log(data);

  return null;
}

function riceLake(comHandler) {
  let line = comHandler.read();
  if (line === null) {
    return null;
  }

  let size = line.length;

  line = line.substring(1, size);
  line = line.trim();
  const message = line.slice(-2);
  line = line.substring(0, size - 2);

  let currentWeight = null;

  if (message === 'LG' || message === 'GZ') {
    // If message is 'GZ', the weight should be 0.
    currentWeight = 0;

    // Otherwise, parse our weight from the line.
    if (message === 'LG') {
      currentWeight = parseInt(line);
    }

    // If our weight is different from the previously recorded weight and we're
    // not currently pushing a value, then return our current weight for pushing.
    if (comHandler.previousWeight !== currentWeight && comHandler.busy === false) {
      return currentWeight;
    }
  }

  // If we don't have an expected message return null.
  return currentWeight;
}

function avery(comHandler) {
  const data = comHandler.read();
  if (data === null) {
    return null;
  }

  // Sometimes we read just the (STX) character and a space, when we see this,
  // do nothing.
  if (data.length === 2) {
    return null;
  }

  let startOfText = data;

  // Data comes in with this special (STX) escape character that I've never seen
  // before. Unfortunately it's in the line randomly. So when it's their we
  // split on it and use everything after it, otherwise we just use the line
  // as is.
  if (startOfText.indexOf('') > -1) {
    startOfText = startOfText.split('')[1];

    if (!startOfText) {
      return null;
    }
  }

  const line = startOfText.trim().split('');

  // Scan through our measurement which contains our weight and an obtuse
  // character code that indicates whether the scale is settling or not.
  const measurement = line.reduce((accumulator, character) => {
    // Dumb trick to check if the character is a number or letter.
    if ('-1234567890'.indexOf(character) !== -1) {
      return { ...accumulator, weight: `${accumulator.weight}${character}` };
    }

    // otherwise it's part of our code.
    return { ...accumulator, code: accumulator.code + character };
  }, { weight: '', code: '' });

  // Finally convert our weight to a number.
  measurement.weight = parseInt(measurement.weight);

  // 'LG' code means the weight has stabilized, so let's use it if so.
  if (measurement.code === 'LG' && measurement.weight !== comHandler.previousWeight) {
    return measurement.weight;
  }

  return null;
}

module.exports = {
  dummy,
  riceLake,
  avery,
};
