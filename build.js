const { compile } = require('nexe');
const fs = require('fs-extra');
const path = require('path');

const builds = [
  { name: 'BigSpringInboundScaleFeeder', config: '{ "username": "KioskUser", "password": "tesl2018", "comPort": "COM7", "scaleId": 1, "parserFormat": 0 }' },
  { name: 'BigSpringOutboundScaleFeeder', config: '{ "username": "KioskUser", "password": "tesl2018", "comPort": "COM5", "scaleId": 2, "parserFormat": 0 }' },
  { name: 'MissionInboundScaleFeeder', config: '{ "username": "MissionKiosk", "password": "Term2018", "comPort": "COM4", "scaleId": 3, "parserFormat": 1 }' },
  { name: 'MissionOutboundScaleFeeder', config: '{ "username": "MissionKiosk", "password": "Term2018", "comPort": "COM5", "scaleId": 4, "parserFormat": 1 }' },
  { name: 'LaSalleInboundScaleFeeder', config: '{ "username": "LaSalleKiosk", "password": "Term2018", "comPort": "COM5", "scaleId": 5, "parserFormat": 0 }' },
  { name: 'LaSalleOutboundScaleFeeder', config: '{ "username": "LaSalleKiosk", "password": "Term2018", "comPort": "COM5", "scaleId": 6, "parserFormat": 0 }' },
  { name: 'DouglasInboundScaleFeeder', config: '{ "username": "DouglasKiosk", "password": "Term2018", "comPort": "COM5", "scaleId": 7, "parserFormat": 0 }' },
  { name: 'DouglasOutboundScaleFeeder', config: '{ "username": "DouglasKiosk", "password": "Term2018", "comPort": "COM5", "scaleId": 8, "parserFormat": 0 }' },
  { name: 'LaredoInboundScaleFeeder', config: '{ "username": "LaredoKiosk", "password": "Term2018", "comPort": "COM5", "scaleId": 9, "parserFormat": 0 }' },
  { name: 'LaredoOutboundScaleFeeder', config: '{ "username": "LaredoKiosk", "password": "Term2018", "comPort": "COM5", "scaleId": 10, "parserFormat": 0 }' },
];

async function build() {
  for (const build of builds) {
    console.log(`Building ${build.name}.`);
    const releaseFolder = path.resolve(__dirname, `releases/${build.name}/`);
    // Clear our release folder if it already exists.
    console.log('Deleting old files.');
    if (fs.existsSync(releaseFolder)) {
      fs.removeSync(releaseFolder);
    }

    // Create our release folder.
    console.log('Creating release folder.');
    fs.mkdirSync(releaseFolder);

    console.log('Generating configuration.');
    const configPath = path.resolve(releaseFolder, 'config.json');
    fs.writeFileSync(configPath, build.config);

    console.log(`Compiling ${build.name}.`);
    await compile({
      input: path.resolve(__dirname, 'ScaleFeeder.js'),
      output: path.resolve(releaseFolder, `${build.name}.exe`),
    });

    console.log(`Done!\n`);
  }
}

build();
